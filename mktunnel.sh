#!/bin/bash


#### THIS SCRIPT MUST BE EXECUTED AS ROOT ####

USER="${1}"
SERVER="${2}"
SSH_CONFIG=~/.ssh/config


# USAGE
if [[ $# -eq 0 ]] ; then
	echo "${0} <user> <server> (e.g. ${0} foo foo.ath.cx)"
	exit 0
fi

# SSH KEY
if [ ! -f ~/.ssh/id_rsa.pub ]; then
	echo ssh-keygen -t rsa
	echo ssh-copy-id "${USER}@${SERVER}"
fi

# AUTOSSH
apt-get update && \
apt-get -y install autossh git 

# CRONTAB
crontab -l > cron.tmp && \
echo "@reboot	/usr/bin/autossh -M 0 -f -T -N tz-ssh-tunnel"	>> cron.tmp && \
crontab cron.tmp && \
crontab -l && \
rm cron.tmp && echo "creating crontab... done."

# SSH CONFIG
cat >> "$SSH_CONFIG" <<'EOF'
Host	tz-ssh-tunnel
		HostName			$SERVER
		User				$USER
		RemoteForward		7002 localhost:22
		LogLevel			INFO
		ServerAliveInterval	30
		ServerAliveCountMax	3
EOF


exit 0
